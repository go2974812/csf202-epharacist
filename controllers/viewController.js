const path = require ('path')

/* LOGIN PAGE */

exports.getLoginForm = (req, res) => {
    res.sendFile(path.join(__dirname,'../','views','login.html'))
};

/*  SIGN UP Page*/
exports.getSignupForm = (req,res) => {
    res.sendFile(path.join(__dirname, '../','views','signup.html'))
};
/*  HOMEPage*/
exports.getHome = (req,res) => {
    res.sendFile(path.join(__dirname, '../','views','dashboard.html'))
};
exports.getProfile = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'myprofilepage.html' ))
}

