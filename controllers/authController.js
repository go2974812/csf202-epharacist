const { JsonWebTokenError } = require('jsonwebtoken')
const User = require('./../models/userModels')
const jwt=require('jsonwebtoken')
const AppError = require ('./../utils/appError')
const promisify = require('util').promisify

const signToken=(id) => {
    return jwt.sign({id}, process.env.JWT_SECRET,{
        expiresIn:process.env.JWT_EXPIRES_IN,
    })
}
exports.login = async (req,res,next)=>{
    try{
        const {email, password} = req.body

        if(!email || !password){
            return next(new AppError('Please provide an email and password!',400))
        }
        //2 if user exists && password is correct
        const user = await User.findOne({email}).select('+password')
        if (!user || !(await user.correctPassword(password, user.password))){
            return next (new AppError('Incorrect password or email',401))
        }
        //if everythings is okey, send token to client
        // const token = signToken (user._id)
        // res.status(200).json({
        //     status:'success',
        //     token,
        createSendToken(user, 200 , res)
        // })
    }
    catch(err){
        res.status(500).json({error:err.message})
    }
}
exports.signup = async (req,res, next)=>{
    try{
        const newUser = await User.create(req.body)
        createSendToken(newUser, 201, res)
    }
    catch(err){
        res.status(500).json({error:err.message})
    }

}
exports.logout = (req, res) => {
    res.cookie('token','',{
        expires: new Date(Date.now() + 10 *1000),
        httpOnly:true,
    })
    res.status(200).json({status : 'success'})
}
const createSendToken = (user, statusCode , res)=>{
    const token = signToken (user._id)
    const cookieOptions = {
        expires: new Date(
            Date.now() + process.env.JWT_COOKIE_EXPIRES_IN *24 *60 *60 *1000,
        ),
        httpOnly:true,

    }
    res.cookie('jwt',token,cookieOptions)
    
    res.status(statusCode).json({
        status: "success",
        token,
        data: {
            user
        }
    })
}
exports.protect = async(req,res,next) =>{
    try{
        //Getting token and check of its there
        let token
        if (
            req.headers.authorization &&
            req.headers.authorization.startsWith('Bearer')
        ){
            token = req.headers.authorization.split(' ')[1]
        }
        else if (req.cookies.jwt){
            token = req.cookies.jwt
        }
        if (!token){
            return next(
                new AppError('You are not logged in! Please log in to get access',401),

            )
        }
        //Verfication token
        const decoded = await promisify(jwt.verify)(token,process.env.JWT_SECRET)
        // console.log(decoded)
        //Check if user still exists
        const freshUser = await User.findById(decoded.id)
        if(!freshUser){
            return next(
                new AppError('The use belonging to this token no longer exist',401),
            )
        }
        //Grant access to protected router
        req.user = freshUser
        next()
    }
    catch(err){
        res.status(500).json({error:err.message})
    }
}
exports.updatePassword = async (req, res, next)=>{
    try{
        //get user from the collection
        const user = await User.findById(req.user.id).select('+password')
        //Check if Posted current password is correct
        if(!(await user.correctPassword(req.body.passwordCurrent, user.password))){
            return next (new AppError('Your current password is wrong',401))

        }
        //If so , update password
        user.password = req.body.password
        user.passwordConfirm = req.body.passwordConfirm
        await user.save()
        //Log user in, send Jwt
        createSendToken(user, 200, res)
    }
    catch(err){
        res.status(500).json({error:err.message});
    }
}
exports.restrictTo=(...roles)=>{
    return(req, res, next)=>{
        if(!roles.includes(req.user.role)){
            return next(
                new AppError('You do not have permission to perform this action', 403),
            )
        }
        next()
    }

}


//Plamning and estimationg with scrum
//1. estimating scale
//2. create baseline

//